import { PrismaDb1Service } from './prisma-db1.service';
import { PrismaDb2Service } from './prisma-db2.service';

export {
    PrismaDb1Service,
    PrismaDb2Service
};