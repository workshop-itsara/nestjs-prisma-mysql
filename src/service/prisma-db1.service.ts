import { Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma-db1/prisma/client';

@Injectable()
export class PrismaDb1Service extends PrismaClient implements OnModuleInit {
    async onModuleInit() {
        await this.$connect();
    }
}