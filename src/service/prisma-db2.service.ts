import { Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma-db2/prisma/client';

@Injectable()
export class PrismaDb2Service extends PrismaClient implements OnModuleInit {
    async onModuleInit() {
        await this.$connect();
    }
}