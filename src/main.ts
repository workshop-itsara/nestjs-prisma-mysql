import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(configService.get('APP_PORT'));
  console.log('🔰 APP_MODE :', configService.get('APP_MODE'));
  console.log('🔰 APP_PORT :', configService.get('APP_PORT'));
}
bootstrap();
