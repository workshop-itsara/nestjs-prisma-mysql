import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { v4 } from 'uuid';
import { PrismaDb1Service, PrismaDb2Service } from '../service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Prisma, users as UsersModel, address as AddressModel } from '@prisma-db1/prisma/client';

@Injectable()
export class UsersService {
  constructor(
    private prismaDb1Service: PrismaDb1Service,
    private prismaDb2Service: PrismaDb2Service
  ) { }

  async getUsers(): Promise<object[]> {
    let queryString = ``;
    const users = await this.prismaDb1Service.users.findMany();

    for (const data of users) {
      //-- Use findMany
      // data['address'] = await this.prismaDb1Service.address.findMany({
      //   select: {
      //     uuid: true,
      //     address: true,
      //     area: true
      //   },
      //   where: {
      //     users_uuid: data.uuid
      //   },
      //   orderBy: {
      //     uuid: 'desc'
      //   }
      // });

      //-- Use $queryRaw
      queryString = `
        SELECT
          uuid,
          address,
          FORMAT((ST_Area(geometry) * 10000000) / 0.8587, 3) AS area,
          ST_AsText(geometry) AS geometry
        FROM address
        WHERE users_uuid = '${data.uuid}'
        ORDER BY uuid DESC
      `;

      data['address'] = await this.prismaDb1Service.$queryRaw(Prisma.raw(queryString));
    }

    return users;
  }

  async createUsers(createUserDto: CreateUserDto): Promise<object> {
    const isUsersExist: UsersModel = await this.prismaDb1Service.users.findFirst({
      where: {
        email: createUserDto.email
      }
    });

    if (isUsersExist) throw new HttpException('Users already exists', HttpStatus.BAD_REQUEST);

    try {
      const usersUuid = v4();
      const createUsers = this.prismaDb1Service.users.create({
        data: {
          uuid: usersUuid,
          email: createUserDto.email,
          name: createUserDto.name
        }
      });

      //-- Use createMany
      // const addressData = [];

      // for (const data of createUserDto.address) {
      //   addressData.push({
      //     address: data.address,
      //     area: data.area,
      //     geometry: data.geometry,
      //     users_uuid: usersUuid
      //   });
      // }

      // const createAddress = this.prismaDb1Service.address.createMany({
      //   data: addressData
      // });

      //-- Use $queryRaw
      let queryString = `INSERT INTO address(uuid, address, area, geometry, users_uuid) VALUES `;

      createUserDto.address.forEach((data, index) => {
        if (index > 0) queryString += `, `;
        queryString += `(UUID(), '${data.address}', ROUND((ST_Area(ST_GeomFromText('${data.geometry}')) * 10000000) / 0.8587, 3), ST_GeomFromText('${data.geometry}'), '${usersUuid}')`;
      });

      const createAddress = this.prismaDb1Service.$queryRaw(Prisma.raw(queryString));

      await this.prismaDb1Service.$transaction([createUsers, createAddress]);

      return {
        message: 'Users created'
      };
    } catch (err) {
      console.log('err', err);
      throw new HttpException(err.meta.message || err.meta.target, HttpStatus.BAD_REQUEST);
    }
  }

  async updateUsers(
    usersUuid: string,
    updateUserDto: UpdateUserDto
  ): Promise<object> {
    const users: UsersModel = await this.prismaDb1Service.users.findFirst({
      where: {
        uuid: usersUuid
      }
    });

    if (!users) throw new HttpException('Users not found', HttpStatus.NOT_FOUND);

    const isUsersEmailExist: UsersModel = await this.prismaDb1Service.users.findFirst({
      where: {
        uuid: { not: usersUuid },
        email: users.email
      }
    });

    if (isUsersEmailExist) throw new HttpException('Email is already', HttpStatus.BAD_REQUEST);

    try {
      const updateUsers = this.prismaDb1Service.users.update({
        where: {
          uuid: usersUuid
        },
        data: {
          email: updateUserDto.hasOwnProperty('email') ? updateUserDto.email : users.email,
          name: updateUserDto.hasOwnProperty('name') ? updateUserDto.name : users.name
        }
      });

      const address: AddressModel[] = await this.prismaDb1Service.address.findMany({
        where: {
          users_uuid: usersUuid
        }
      });

      const updateAddress = [];
      const addrerssUuid = [];
      const deleteAddressUuid = [];
      let queryString = ``;
      let addressIndex = 0;

      updateUserDto.address.forEach((data) => {
        if (data.hasOwnProperty('uuid') && data.uuid) addrerssUuid.push(data.uuid);
      });

      address.forEach((data) => {
        if (!addrerssUuid.includes(data.uuid)) deleteAddressUuid.push(data.uuid);
      });

      if (deleteAddressUuid.length) {
        queryString = `DELETE FROM address WHERE uuid IN (`;

        deleteAddressUuid.forEach((uuid, index) => {
          if (index > 0) queryString += `, `;
          queryString += `'${uuid}'`;
        });

        queryString += `);`;
        updateAddress.push(this.prismaDb1Service.$queryRaw(Prisma.raw(queryString)));
      }

      for (const data of updateUserDto.address) {
        addressIndex = address.map((obj) => obj.uuid).indexOf(data.uuid);

        if (addressIndex === -1) {
          queryString = `
            INSERT INTO address(uuid, address, area, geometry, users_uuid) VALUES
            (UUID(), '${data.address}', ((ST_Area(ST_GeomFromText('${data.geometry}')) * 10000000) / 0.8587), ST_GeomFromText('${data.geometry}'), '${usersUuid}');`;
          updateAddress.push(this.prismaDb1Service.$queryRaw(Prisma.raw(queryString)));
        } else {
          queryString = `
            UPDATE address SET
              address = '${data.hasOwnProperty('address') ? data.address : address[addressIndex].address}' `;

          if (data.hasOwnProperty('geometry')) {
            queryString += `
              , area = ((ST_Area(ST_GeomFromText('${data.geometry}')) * 10000000) / 0.8587)
              , geometry = ST_GeomFromText('${data.geometry}') `;
          }

          queryString += `WHERE uuid = '${address[addressIndex].uuid}';`;
          updateAddress.push(this.prismaDb1Service.$queryRaw(Prisma.raw(queryString)));
        }
      }

      await this.prismaDb1Service.$transaction([updateUsers, ...updateAddress]);

      return {
        message: 'Users updated'
      };
    } catch (err) {
      console.log('err', err);
      throw new HttpException(err.meta.message || err.meta.target, HttpStatus.BAD_REQUEST);
    }
  }

  async deleteUsers(usersUuid: string): Promise<object> {
    const isUsersExist: UsersModel = await this.prismaDb1Service.users.findFirst({
      where: {
        uuid: usersUuid
      }
    });

    if (!isUsersExist) throw new HttpException('Users not found', HttpStatus.NOT_FOUND);

    try {
      const deleteUsers = this.prismaDb1Service.users.delete({
        where: {
          uuid: usersUuid
        }
      });

      //-- Use delete
      // const deleteAddress = this.prismaDb1Service.address.delete({
      //   where: {
      //     users_uuid: usersUuid
      //   }
      // });

      //-- Use $queryRaw
      let queryString = `DELETE FROM address WHERE users_uuid = '${usersUuid}'`;
      const deleteAddress = this.prismaDb1Service.$queryRaw(Prisma.raw(queryString));

      await this.prismaDb1Service.$transaction([deleteAddress, deleteUsers]);

      return {
        message: 'Users deleted'
      };
    } catch (err) {
      console.log('err', err);
      throw new HttpException(err.meta.message || err.meta.target, HttpStatus.BAD_REQUEST);
    }
  }
}