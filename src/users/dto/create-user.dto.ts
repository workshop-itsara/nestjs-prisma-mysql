import { Type } from "class-transformer";
import { ArrayMinSize, IsArray, IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID, ValidateNested } from "class-validator";

class AddressDto {
    @IsOptional()
    @IsUUID()
    uuid: string;

    @IsNotEmpty()
    address: string

    @IsOptional()
    @IsNumber()
    area: number;

    @IsNotEmpty()
    @IsString()
    geometry: string
}

export class CreateUserDto {
    @IsEmail()
    email: string;

    @IsOptional()
    @IsNotEmpty()
    name: string;

    @IsArray()
    @ArrayMinSize(1)
    @Type(() => AddressDto)
    @ValidateNested()
    address: AddressDto[]
}