import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { PrismaDb1Service, PrismaDb2Service } from '../service';

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        PrismaDb1Service,
        PrismaDb2Service
      ]
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});