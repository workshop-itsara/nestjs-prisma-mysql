import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { PrismaDb1Service, PrismaDb2Service } from '../service';

@Module({
  controllers: [UsersController],
  providers: [
    UsersService,
    PrismaDb1Service,
    PrismaDb2Service
  ]
})
export class UsersModule { }