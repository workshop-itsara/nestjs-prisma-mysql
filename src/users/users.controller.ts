import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Get('get-users')
  async getUsers(): Promise<object[]> {
    return await this.usersService.getUsers();
  }

  @Post('create-users')
  async createUsers(@Body() createUserDto: CreateUserDto): Promise<object> {
    return await this.usersService.createUsers(createUserDto);
  }

  @Patch('update-users/:usersUuid')
  async updateUsers(
    @Param('usersUuid') usersUuid: string,
    @Body() updateUserDto: UpdateUserDto
  ): Promise<object> {
    return await this.usersService.updateUsers(usersUuid, updateUserDto);
  }

  @Delete('delete-users/:usersUuid')
  async deleteUsers(@Param('usersUuid') usersUuid: string): Promise<object> {
    return await this.usersService.deleteUsers(usersUuid);
  }
}